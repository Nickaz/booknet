from invoke import task


@task
def run(context):
    context.run('docker-compose up -d postgres && python manage.py runserver')


@task
def isort(context, params="apps config"):
    context.run(f'isort {params}')


@task
def pep8(context, params="apps config"):
    context.run(f'flake8 {params}')


@task
def deploy(context):
    context.run('git push heroku main')


@task
def shell(context):
    context.run('python manage.py shell_plus')
