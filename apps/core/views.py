from collections import namedtuple

from django.conf import settings
from django.urls import reverse_lazy
from django.views.generic import TemplateView

from utils.changelog import get_changelog_html, get_latest_version

Changelog = namedtuple('Changelog', ['name', 'text', 'version', 'open_api_ui'])


class IndexView(TemplateView):
    """Class-based view for home page."""
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        """Load changelog data from files."""
        context = super().get_context_data(**kwargs)
        context['version'] = get_latest_version('changelog.md'),
        context['changelog'] = Changelog(
            name=settings.SPECTACULAR_SETTINGS.get('TITLE'),
            text=get_changelog_html('changelog.md'),
            version=settings.SPECTACULAR_SETTINGS.get('VERSION'),
            open_api_ui=reverse_lazy('schema-swagger-ui'),
        )
        return context
