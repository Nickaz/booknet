from rest_framework.permissions import IsAuthenticatedOrReadOnly


class ActionPermissionsMixin:
    """Mixin which allows to define specific permissions per actions."""

    permissions_map = None

    def get_permissions(self):
        """Returns permissions list for current ``.action`` attribute value

        Returns:
            list: list of permissions for related action
        """
        assert self.permissions_map is not None, (
            '"{0}" should either include ``permissions_map`` '
            'attribute'.format(self.__class__.__name__)
        )
        if self.action in self.permissions_map:
            permission_classes = self.permissions_map[self.action]
        else:
            permission_classes = self.permissions_map.get(
                'default',
                (IsAuthenticatedOrReadOnly, )
            )

        return [permission() for permission in permission_classes]
