from django.db import models


class TradeOffer(models.Model):

    class StatusChoices(models.TextChoices):
        completed = 'completed', 'Completed'
        active = 'active', 'Active'
        rejected = 'rejected', 'Rejected'
        canceled = 'canceled', 'Canceled'

    sender = models.ForeignKey(
        to='users.User',
        on_delete=models.CASCADE,
        verbose_name='Offer Sender',
        related_name='sent_offers',
    )
    receiver = models.ForeignKey(
        to='users.User',
        on_delete=models.CASCADE,
        verbose_name='Offer Receiver',
        related_name='received_offers'
    )
    suggested_books = models.ManyToManyField(
        to='books.Book',
        related_name='as_suggested',
        verbose_name='Suggested books',
    )
    requested_books = models.ManyToManyField(
        to='books.Book',
        related_name='as_requested',
        verbose_name='Requested books',
    )
    message = models.TextField(
        verbose_name='Message',
        max_length=1024,
        default=''
    )
    status = models.CharField(
        verbose_name='Status',
        max_length=24,
        choices=StatusChoices.choices,
        default=StatusChoices.active,
    )

    def __str__(self) -> str:
        return f'Trade from {self.sender} to {self.receiver}'
