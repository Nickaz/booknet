from django.contrib import admin

from .models import TradeOffer


@admin.register(TradeOffer)
class TradeOfferAdmin(admin.ModelAdmin):
    search_fields = [
        'sender',
        'receiver',
        'message',
    ]
    list_display = (
        'id',
        'status',
        'sender',
        'receiver',
    )
    list_display_links = (
        'id',
        'status',
    )

