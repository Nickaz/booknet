from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from ..models import TradeOffer
from .serializers import TradeOfferSerializer


class TradeOfferViewSet(ModelViewSet):

    queryset = TradeOffer.objects.prefetch_related(
        'suggested_books', 'requested_books'
    )
    serializer_class = TradeOfferSerializer
    permission_classes = [IsAuthenticated]
