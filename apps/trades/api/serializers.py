from rest_framework.serializers import ModelSerializer

from apps.books.api.serializers import BookSerializer
from apps.users.api.serializers import UserSerializer

from ..models import TradeOffer


class TradeOfferSerializer(ModelSerializer):

    sender_data = UserSerializer(source='sender', read_only=True)
    receiver_data = UserSerializer(source='receiver', read_only=True)
    suggested_books_data = BookSerializer(
        source='suggested_books',
        read_only=True,
        many=True,
    )
    requested_books_data = BookSerializer(
        source='requested_books',
        read_only=True,
        many=True,
    )

    class Meta:
        model = TradeOffer
        fields = (
            'id',
            'sender',
            'sender_data',
            'receiver',
            'receiver_data',
            'suggested_books',
            'suggested_books_data',
            'requested_books',
            'requested_books_data',
            'message',
            'status',
        )
        extra_kwargs = {
            'suggested_books': {'allow_empty': True},
            'requested_books': {'allow_empty': True},
        }
