from rest_framework.routers import DefaultRouter

from .views import TradeOfferViewSet

app_name = 'trades'

router = DefaultRouter()
router.register('', TradeOfferViewSet)
router.register(
    '',
    TradeOfferViewSet,
    basename='trade-offers'
)

urlpatterns = router.urls
