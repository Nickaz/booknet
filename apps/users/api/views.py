from django.db.models.query import Q

from rest_framework import filters, status
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from drf_yasg.utils import swagger_auto_schema

from apps.books.api.serializers import BookSerializer
from apps.core.api.mixins import ActionPermissionsMixin
from apps.trades.api.serializers import TradeOfferSerializer
from apps.trades.models import TradeOffer

from ..permissions import IsProfileOwner
from ..models import User
from .serializers import (
    PasswordResetConfirmSerializer,
    PasswordResetSerializer,
    RegistrationSerializer,
    UserSerializer,
)


class RegistrationAPIView(GenericAPIView):
    """Register user in system."""
    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class PasswordResetView(GenericAPIView):
    """
    Calls Django Auth PasswordResetForm save method.
    Accepts the following POST parameters: email
    Returns the success/fail message.
    """

    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        # Create a serializer with request.data
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.save()
        # Return the success message with OK HTTP status
        return Response(
            {"success": "Password reset e-mail has been sent."},
            status=status.HTTP_200_OK
        )


class PasswordResetConfirmView(GenericAPIView):
    """
    Password reset e-mail link is confirmed, therefore this resets the user's password.
    Accepts the following POST parameters: new_password1, new_password2
    Accepts the following Django URL arguments: token, id
    Returns the success/fail message.
    """

    serializer_class = PasswordResetConfirmSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            {"success": "Password has been reset with the new password."}
        )


class UserInfoView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user


class ProfileViewSet(ActionPermissionsMixin, ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permissions_map = {
        'create': (IsProfileOwner,),
        'update': (IsProfileOwner,),
        'destroy': (IsProfileOwner,),
        'default': (IsAuthenticated,)
    }

    filter_backends = [filters.SearchFilter]
    search_fields = [
        'first_name',
        'last_name',
        'email',
    ]

    @swagger_auto_schema(method='GET', responses={200: BookSerializer(many=True)})
    @action(detail=True, methods=['GET'], url_path='books')
    def user_books(self, request, *args, **kwargs):
        user = self.get_object()

        queryset = user.books.all()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = BookSerializer(
                page,
                many=True,
                context=self.get_serializer_context(),
            )
            return self.get_paginated_response(serializer.data)

        serializer = self.BookSerializer(
            queryset,
            many=True,
            context=self.get_serializer_context(),
        )
        return Response(serializer.data)

    @swagger_auto_schema(method='GET', responses={200: TradeOfferSerializer(many=True)})
    @action(detail=True, methods=['GET'], url_path='trades')
    def trade_offers(self, request, *args, **kwargs):
        """Return `TradeOffer`s that was sended or received by user."""
        user = self.get_object()

        qs = TradeOffer.objects.filter(
            Q(sender=user) | Q(receiver=user)
        ).prefetch_related('suggested_books', 'requested_books')

        page = self.paginate_queryset(qs)
        if page is not None:
            serializer = TradeOfferSerializer(
                page,
                many=True,
                context=self.get_serializer_context(),
            )
            return self.get_paginated_response(serializer.data)

        serializer = self.TradeOfferSerializer(
            qs,
            many=True,
            context=self.get_serializer_context(),
        )
        return Response(serializer.data)
