from rest_framework.routers import DefaultRouter

from .api.views import ProfileViewSet

app_name = 'users'

router = DefaultRouter()
router.register('', ProfileViewSet)

urlpatterns = router.urls
