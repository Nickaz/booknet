from rest_framework.permissions import SAFE_METHODS, IsAuthenticatedOrReadOnly


class IsAuthorOrReadOnly(IsAuthenticatedOrReadOnly):
    """Check if user is the author of comment."""

    def has_object_permission(self, request, view, obj):
        """Return `True` if user if comment author."""
        return bool(
            obj.author == request.user or
            request.method in SAFE_METHODS
        )


class CanDeleteComment(IsAuthorOrReadOnly):
    """Check if user has permission to delete comment."""

    def has_object_permission(self, request, view, obj):
        """Only comment author or author of discussion can delete comment."""
        is_comment_author = super().has_object_permission(request, view, obj)
        return is_comment_author or obj.discussion.author == request.user
