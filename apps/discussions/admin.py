from django.contrib import admin

from .models import Comment, Discussion


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass


@admin.register(Discussion)
class DiscussionAdmin(admin.ModelAdmin):
    pass
