from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DiscussionsAppConfig(AppConfig):
    """Default configuration for Discussions app."""
    name = "apps.discussions"
    verbose_name = _("Discussions")
