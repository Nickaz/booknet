from rest_framework import serializers

from ...users.api.serializers import UserSerializer
from ..models import Comment, Discussion


class CommentSerializer(serializers.ModelSerializer):

    author = serializers.SerializerMethodField(read_only=True)
    rating = serializers.ReadOnlyField()

    class Meta:
        model = Comment
        fields = (
            'id',
            'message',
            'discussion',
            'rating',
            'author',
            'created',
            'modified',
        )

    def get_author(self, obj) -> UserSerializer:
        return UserSerializer(obj.author).data


class DiscussionSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField(read_only=True)
    comments = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = Discussion
        fields = (
            'id',
            'title',
            'content',
            'comments',
            'author',
        )

    def get_author(self, obj) -> UserSerializer:
        return UserSerializer(obj.author).data
