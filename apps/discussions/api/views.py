from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from drf_yasg.utils import swagger_auto_schema

from apps.core.api.mixins import ActionPermissionsMixin

from ..models import Comment, Discussion
from ..permissions import CanDeleteComment, IsAuthorOrReadOnly
from .serializers import CommentSerializer, DiscussionSerializer


class DiscussionViewSet(ModelViewSet):
    """ViewSet for discussions."""
    queryset = Discussion.objects.all()
    serializer_class = DiscussionSerializer
    permission_classes = (IsAuthorOrReadOnly, )

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @swagger_auto_schema(paginator_inspectors=None)
    @action(detail=False, methods=['GET'], url_path='most-commented')
    def most_commented(self, request):
        """Get ten most commented discussions."""
        discussions = self.get_queryset().most_commented()
        serializer = self.get_serializer(discussions, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'], url_path='my')
    def users_discussions(self, request):
        """Get user's discussions."""
        discussions = self.get_queryset().filter(author=request.user)
        serializer = self.get_serializer(discussions, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['GET'], url_path='commented-by-user')
    def commented_by_user_discussions(self, request):
        """Get discussions which was commented by user."""
        discussions = self.get_queryset().filter(
            comments__author=request.user
        ).distinct()
        serializer = self.get_serializer(discussions, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)


class CommentViewSet(ActionPermissionsMixin, ModelViewSet):
    """ViewSet for discussion comments."""
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    base_permissions = (IsAuthenticated, )
    permissions_map = {
        'list': base_permissions,
        'retrieve': base_permissions,
        'create': base_permissions,
        'update': base_permissions + (IsAuthorOrReadOnly, ),
        'partial_update': base_permissions + (IsAuthorOrReadOnly, ),
        'delete': base_permissions + (CanDeleteComment, ),
    }

    def get_queryset(self):
        return super().get_queryset().filter(
            discussion=self.kwargs.get('discussion_pk')
        )

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
