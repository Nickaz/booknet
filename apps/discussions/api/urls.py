from django.urls import include, path

from rest_framework.routers import SimpleRouter

from rest_framework_nested.routers import NestedSimpleRouter

from .views import CommentViewSet, DiscussionViewSet

router = SimpleRouter()
router.register('', DiscussionViewSet)

discussion_router = NestedSimpleRouter(
    router,
    r'',
    lookup='discussion'
)
discussion_router.register(
    r'comments',
    CommentViewSet,
    basename='discussion-comments'
)

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'', include(discussion_router.urls)),
]
