from django.db import models

from model_utils.models import TimeStampedModel

from apps.users.models import User


class DiscussionQuerySet(models.QuerySet):
    """Queryset for `Discussion` model."""

    def most_commented(self):
        """Return 10 most commented discussions."""
        return self.annotate(
            c_count=models.Count('comments')
        ).order_by('-c_count')[:10]


class Discussion(TimeStampedModel):
    title = models.CharField(
        verbose_name='Title',
        max_length=64,
        null=False,
        default='',
    )
    author = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='own_discussions',
        verbose_name='Author',
    )
    content = models.TextField(
        verbose_name='Content',
        max_length=512,
        null=False,
        default='',
    )
    attendees = models.ManyToManyField(
        to=User,
        related_name='discussions',
    )

    objects = DiscussionQuerySet.as_manager()

    def __str__(self):
        return f'{self.title} (A: {self.attendees.count()}; C: {self.comments.count()})'


class Comment(TimeStampedModel):
    author = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='comments',
        verbose_name='Author',
    )
    discussion = models.ForeignKey(
        to=Discussion,
        on_delete=models.CASCADE,
        related_name='comments',
        verbose_name='Discussion',
    )
    message = models.TextField(
        verbose_name='Message',
        max_length=512,
        blank=True,
        null=False,
        default='',
    )
    rating = models.IntegerField(
        default=0,
    )

    def __str__(self):
        return f'{self.author}: {self.message}'
