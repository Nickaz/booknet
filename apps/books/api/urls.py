from rest_framework.routers import SimpleRouter

from .views import BookViewSet, LibraryViewSet

router = SimpleRouter()
router.register('', BookViewSet)
router.register(
    'library',
    LibraryViewSet,
    basename='user_library'
)

urlpatterns = router.urls
