from statistics import mean

from rest_framework import serializers

from ...users.api.serializers import UserSerializer
from ...users.models import User
from ..models import Book, Review, UserBook


class AddBookSerializer(serializers.Serializer):
    book_id = serializers.IntegerField()
    status = serializers.ChoiceField(
        choices=UserBook.Status.choices,
        default=UserBook.Status.READING,
    )


class BookSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()
    genre = serializers.StringRelatedField()
    status = serializers.SerializerMethodField()
    average_rating = serializers.SerializerMethodField()
    reviews_count = serializers.SerializerMethodField()
    want_to_read_count = serializers.SerializerMethodField()
    want_to_trade_count = serializers.SerializerMethodField()

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'author',
            'genre',
            'pages',
            'status',
            'viewers_count',
            'readers_count',
            'description',
            'average_rating',
            'reviews_count',
            'want_to_read_count',
            'want_to_trade_count',
        )

    def get_average_rating(self, instance) -> float:
        ratings = instance.user_books.exclude(
            review__mark__isnull=True
        ).values_list('review__mark', flat=True)

        if not ratings or ratings.first() is None:
            return 0
        return mean(list(ratings))

    def get_reviews_count(self, instance) -> int:
        return instance.user_books.exclude(review__isnull=True).exclude(
            review__feedback__exact=''
        ).count()

    def get_want_to_read_count(self, instance) -> int:
        return instance.user_books.filter(
            status=UserBook.Status.WANNA_READ
        ).count()

    def get_want_to_trade_count(self, instance) -> int:
        return instance.user_books.filter(
            status=UserBook.Status.WANNA_TRADE
        ).count()

    def get_status(self, instance):
        default_status = ''

        user = self.context.get('user')
        if user:
            user_books = UserBook.objects.filter(
                user=user, book=instance
            )
            if user_books.exists():
                return user_books.first().status

        return default_status


class ReviewSerializer(serializers.ModelSerializer):

    user = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Review
        fields = (
            'mark',
            'feedback',
            'user'
        )

    def get_user(self, obj: Review) -> User:
        user = obj.user_books.first().user
        return UserSerializer(user).data


class StatusSerializer(serializers.Serializer):
    status = serializers.ChoiceField(choices=UserBook.Status.choices)
