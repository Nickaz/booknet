from django.shortcuts import get_object_or_404

from rest_framework import filters, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from drf_yasg.utils import swagger_auto_schema

from ...core.api.pagination import BasePageNumberPagination
from ...users.api.serializers import UserSerializer
from ...users.models import User
from ..models import Book, Review, UserBook
from .serializers import AddBookSerializer, BookSerializer, ReviewSerializer, StatusSerializer


class BookViewSet(ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (IsAuthenticated, )
    filter_backends = [filters.SearchFilter]
    pagination_class = BasePageNumberPagination
    search_fields = ['title', 'author__full_name']
    lookup_value_regex = r'[0-9]+'

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    def retrieve(self, request, *args, **kwargs):
        book = self.get_object()
        book.viewers.add(self.request.user)
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(paginator_inspectors=None)
    @action(detail=False, methods=['GET'], url_path='most-viewed')
    def most_viewed(self, request):
        books = self.get_queryset().most_viewed()
        serializer = self.get_serializer(books, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={200: ReviewSerializer}, request_body=ReviewSerializer)
    @action(detail=True, methods=['POST'])
    def add_feedback(self, request, *args, **kwargs):

        book = self.get_object()

        review = Review.objects.create(
            mark=self.request.data.get('mark'),
            feedback=self.request.data.get('feedback'),
        )
        user_book, _ = UserBook.objects.get_or_create(
            user=self.request.user,
            book=book,
        )
        user_book.review = review
        user_book.save()

        serializer = ReviewSerializer(user_book.review)
        return Response(data=serializer.data)

    @swagger_auto_schema(responses={200: ReviewSerializer})
    @action(detail=True, methods=['GET'])
    def reviews(self, request, *args, **kwargs):
        book = self.get_object()

        # TODO: Refactor this to improve performance
        reviews = [
            user_book.review for user_book in book.user_books.filter(book=book)
            if user_book.review
        ]
        serializer = ReviewSerializer(reviews, many=True)

        return Response(data=serializer.data)

    @swagger_auto_schema(responses={200: UserSerializer})
    @action(detail=True, methods=['GET'], url_path='want-to-read')
    def want_to_read(self, request, *args, **kwargs):
        book = self.get_object()

        want_to_read = book.user_books.filter(
            status=UserBook.Status.WANNA_READ
        ).values_list('user', flat=True)
        users = User.objects.filter(id__in=want_to_read)

        return Response(
            data=UserSerializer(instance=users, many=True).data
        )

    @swagger_auto_schema(responses={200: UserSerializer})
    @action(detail=True, methods=['GET'], url_path='want-to-trade')
    def want_to_trade(self, request, *args, **kwargs):
        book = self.get_object()

        want_to_read = book.user_books.filter(
            status=UserBook.Status.WANNA_TRADE
        ).values_list('user', flat=True)
        users = User.objects.filter(id__in=want_to_read)

        return Response(
            data=UserSerializer(instance=users, many=True).data
        )

    @swagger_auto_schema(request_body=StatusSerializer)
    @action(detail=True, methods=['PATCH'], url_path='change-status')
    def change_status(self, request, *args, **kwargs):
        book = self.get_object()
        serializer = StatusSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_book, _ = UserBook.objects.get_or_create(user=request.user, book=book)
        user_book.status = serializer.data['status']
        user_book.save()

        return Response(status=status.HTTP_200_OK)


class LibraryViewSet(ReadOnlyModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.prefetch_related('user_books').filter(
            user_books__user=self.request.user.id
        )

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user'] = self.request.user
        return context

    @swagger_auto_schema(request_body=AddBookSerializer)
    @action(methods=['POST'], detail=False)
    def add(self, request, *args, **kwargs):
        book_id = request.data.get('book_id')
        status = request.data.get('status')

        book = get_object_or_404(Book.objects.all(), id=book_id)
        user_book, _ = UserBook.objects.get_or_create(
            user=self.request.user,
            book=book,
        )
        user_book.status = status
        user_book.save()

        return Response(
            data=self.get_serializer(book).data,
            status=HTTP_200_OK,
        )

    @action(methods=['POST'], detail=False, url_path='create')
    def create_new_book(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        book = serializer.save()
        user_book, _ = UserBook.objects.get_or_create(
            user=self.request.user,
            book=book,
        )
        status = request.data.get('status')
        user_book.status = status
        user_book.save()

        return Response(data=serializer.data)

    def destroy(self, request, *args,):
        """Remove the book from user library."""
        book = self.get_object()
        UserBook.objects.get(
            book=book,
            user=self.request.user
        ).delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
