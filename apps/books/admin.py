from django.contrib import admin

from .models import Author, Book, Genre, Review, UserBook


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    search_fields = [
        'title',
    ]
    list_display = (
        'id',
        'title',
        'author',
        'genre',
    )


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    pass


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    pass


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    pass


@admin.register(UserBook)
class UserBookAdmin(admin.ModelAdmin):
    pass
