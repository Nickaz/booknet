from django.db import models


class Author(models.Model):
    full_name = models.TextField(
        max_length=120,
        verbose_name='Author',
    )

    def __str__(self):
        return self.full_name


class Genre(models.Model):
    name = models.TextField(
        max_length=120,
        verbose_name='Genre',
    )

    def __str__(self):
        return self.name


class Review(models.Model):
    mark = models.IntegerField(
        verbose_name='Mark',
    )
    feedback = models.TextField(
        max_length=256,
        verbose_name='Feedback',
        blank=True,
        default='',
    )

    def __str__(self):
        return f'Review (Mark {self.mark})'


class BookQuerySet(models.QuerySet):
    """Queryset for `Book` model."""

    def most_viewed(self):
        return self.annotate(v_count=models.Count('viewers')).order_by('-v_count')[:10]


class Book(models.Model):
    title = models.TextField(
        max_length=64,
        verbose_name='Book',
        blank=True,
        default='',
    )
    description = models.TextField(
        max_length=512,
        verbose_name='Description',
        blank=True,
        default='',
    )
    author = models.ForeignKey(
        to=Author,
        on_delete=models.SET_NULL,
        related_name='books',
        null=True,
    )
    genre = models.ForeignKey(
        to=Genre,
        on_delete=models.SET_NULL,
        related_name='books',
        null=True,
    )
    pages = models.IntegerField(
        verbose_name='Pages',
        null=True,
        blank=True,
    )

    objects = BookQuerySet.as_manager()

    def __str__(self):
        return f'{self.title} by {self.author.full_name if self.author else ""}'

    @property
    def viewers_count(self) -> int:
        return self.viewers.count()

    @property
    def readers_count(self):
        return self.users.count()


class UserBook(models.Model):
    user = models.ForeignKey(
        to='users.User',
        on_delete=models.CASCADE,
        related_name='user_books',
    )
    book = models.ForeignKey(
        to=Book,
        on_delete=models.RESTRICT,
        related_name='user_books',
    )
    review = models.ForeignKey(
        to=Review,
        on_delete=models.SET_NULL,
        related_name='user_books',
        null=True,
        blank=True,
    )

    class Status(models.TextChoices):
        FINISH = 'finish'
        WANNA_READ = 'want_to_read'
        WANNA_TRADE = 'want_to_trade'
        NOT_INTERESTED = 'not_interested'
        READING = 'reading'

    status = models.TextField(
        choices=Status.choices,
        default=Status.WANNA_READ,
        verbose_name='Status',
    )

    def __str__(self):
        return f'{self.user} {self.book}'

    class Meta:
        unique_together = ('user', 'book')
