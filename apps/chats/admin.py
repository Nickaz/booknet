from django.contrib import admin

from .models import Dialog, Message


@admin.register(Dialog)
class DialogAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )
    list_display_links = (
        'id',
        'name',
    )


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'sender',
        'dialog',
    )
    list_display_links = (
        'id',
        'sender',
        'dialog',
    )
