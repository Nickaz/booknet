from rest_framework import permissions

from .models import Dialog


class IsChatAttendee(permissions.IsAuthenticated):

     def has_object_permission(self, request, view, obj: Dialog):
         """Check if user in attendees."""
         return request.user in obj.attendees.all()
