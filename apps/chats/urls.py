# chat/urls.py
from django.urls import path, re_path

from . import views
from .consumers import consumers

urlpatterns = [
    path('', views.index, name='index'),
    path('<str:room_name>/', views.room, name='room'),
    re_path(r'ws/chat/(?P<dialog_id>\w+)/$', consumers.ChatConsumer.as_asgi()),
]