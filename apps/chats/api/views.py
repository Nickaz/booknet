from rest_framework.viewsets import ModelViewSet

from apps.core.api.mixins import ActionPermissionsMixin

from ..models import Dialog
from ..permissions import IsChatAttendee
from .serializers import DialogSerializer


class DialogViewSet(ActionPermissionsMixin, ModelViewSet):
    queryset = Dialog.objects.all()
    serializer_class = DialogSerializer
    permissions_map = {
        'default': (IsChatAttendee,)
    }

    def get_queryset(self):
        return Dialog.objects.filter(
            attendees=self.request.user.id,
        ).prefetch_related('messages', 'attendees')
