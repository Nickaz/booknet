from django.urls import path

from rest_framework.routers import DefaultRouter

from .views import DialogViewSet

app_name = 'chats'

router = DefaultRouter()
router.register('', DialogViewSet)

urlpatterns = router.urls