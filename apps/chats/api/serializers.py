from os.path import normcase

from rest_framework import serializers

from apps.users.api.serializers import UserSerializer

from ..models import Dialog, Message


class MessageSerializer(serializers.ModelSerializer):
    """Serializer for Message model."""
    sender_data = UserSerializer(source='sender')

    class Meta:
        model = Message
        fields = (
            'id',
            'sender',
            'sender_data',
            'content',
            'created',
        )


class DialogSerializer(serializers.ModelSerializer):
    """Serializer for Dialog models."""
    attendees_data = UserSerializer(
        source='attendees',
        many=True,
        read_only=True,
    )
    messages = serializers.SerializerMethodField()

    class Meta:
        model = Dialog
        fields = (
            'id',
            'name',
            'image',
            'attendees',
            'attendees_data',
            'messages',
        )

    def get_messages(self, obj):
        messages = obj.messages.all()[:20]
        return MessageSerializer(messages, many=True).data
