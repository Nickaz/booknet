from django.db import models


class Dialog(models.Model):
    name = models.CharField(
        verbose_name='Dialog name',
        max_length=64,
    )
    image = models.ImageField(
        verbose_name='Dialog Icon',
        blank=True,
        null=True,
    )
    attendees = models.ManyToManyField(
        to='users.User',
        related_name='dialogs',
        verbose_name='Attendees',
    )

    def __str__(self) -> str:
        return f'<Dialog> {self.name}'


class Message(models.Model):
    sender = models.ForeignKey(
        to='users.User',
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='User',
    )
    dialog = models.ForeignKey(
        to=Dialog,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Dialog',
    )
    content = models.TextField(
        verbose_name='Message content',
    )
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)
