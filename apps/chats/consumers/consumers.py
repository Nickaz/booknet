# chat/consumers.py
import json

from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from apps.users.models import User

from ..models import Dialog, Message


class ChatConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.dialog_id = self.scope['url_route']['kwargs']['dialog_id']
        self.dialog = await self.get_dialog(int(self.dialog_id))

        await self.channel_layer.group_add(
            self.dialog.name,
            self.channel_name,
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.dialog.name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        data = json.loads(text_data)
        message = data['message']
        user_id = data['sender_id']
        dialog_id = data['dialog_id']

        await self.save_message(
            user_id=int(user_id),
            dialog_id=int(dialog_id),
            message=message,
        )

        dialog = await self.get_dialog(int(dialog_id))

        # Send message to room group
        await self.channel_layer.group_send(
            self.dialog.name,
            {
            'type': 'chat_message',
            'message': message,
            'sender_id': user_id,
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        user_id = event['sender_id']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'sender_id': user_id,
        }))

    @sync_to_async
    def save_message(self, user_id: int, dialog_id: int, message):
        user = User.objects.get(id=user_id)
        dialog = Dialog.objects.get(id=dialog_id)
        Message.objects.create(
            sender=user,
            dialog=dialog,
            content=message
        )

    @sync_to_async
    def get_dialog(self, dialog_id: int):
        return Dialog.objects.get(id=dialog_id)
