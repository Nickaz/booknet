from django.urls import include, path


urlpatterns = [
    path('books/', include('apps.books.api.urls')),
    path('discussions/', include('apps.discussions.api.urls')),
    path('auth/', include('apps.users.api.urls', namespace='auth')),
    path('trades/', include('apps.trades.api.urls', namespace='trades')),
    path('profiles/', include('apps.users.urls', namespace='profiles')),
    path('dialogs/', include('apps.chats.api.urls', namespace='chats')),
]
